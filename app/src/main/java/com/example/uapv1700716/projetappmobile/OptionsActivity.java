package com.example.uapv1700716.projetappmobile;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class OptionsActivity extends AppCompatActivity {

        RadioGroup radioGroup;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_options);

            Spinner spinner = (Spinner) findViewById(R.id.choixSpinner);
            String[] lRegion={"L1 Informatique","L2 Informatique","L3 Informatique"};
            ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,lRegion);
            dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(dataAdapterR);


            Button savePrefButton = (Button) findViewById(R.id.savePrefButton);



            savePrefButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {




                    String choixLicense = spinner.getSelectedItem().toString();

                    radioGroup = findViewById(R.id.radioGroup);

                    int groupeSelected = 0;

                    Snackbar.make(v, "Préférences enregistrés", 100).show();

                    RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());

                    switch (radioButton.getText().toString()) {
                        case "Groupe 1" : groupeSelected = 1; break;
                        case "Groupe 2" : groupeSelected = 2; break;
                        case "Groupe 3" : groupeSelected = 3; break;
                        case "Groupe 4" : groupeSelected = 4; break;
                    }

                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();

                    editor.putInt("Groupe", groupeSelected);        // Saving integer
                    editor.putString("choixLicense", choixLicense);  // Saving string

                    editor.commit(); // commit changes


                }
            });
        }

}
