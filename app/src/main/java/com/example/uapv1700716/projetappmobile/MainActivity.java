package com.example.uapv1700716.projetappmobile;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceActivity;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;


import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

public class MainActivity extends AppCompatActivity {


    private Database db;
    static public Map<String, Object> nextCourse;
    // Variables pour récuéprer les extras
    String choixLicense;
    int groupe;

    // Les buttons
    Button gotoCalButton;
    Button gotoExaButton;
    Button gotoPrefButton;
    /*
    J'ai enlevé le button pour revenir à la sélection car on peut utiliser le button retour de base et que ça peut foirer le programme en empilant des instances des activités
     */
    //Button gotoSelButton;




    // Variable boolean pour savoir si le cours existe, s'il n'y a pas de cours à l'horaire donnée, elle est false
    // Vu que mon emploi du temps par jour n'est pas modulable, il faut parcourir toutes les cases d'horaire à chaque fois
    // Si le cours existe à l'horaire, il faut passer coursARemplir à true et récupérer les info de la database, sinon juste passer coursARemplir a false
    public boolean coursARemplir;

    // Ce sont toutes les data que tu dois saisir depuis la database pour une horaire précise
    // Il n'y a pas besoin de l'heure car c'est déjà préremplis
    // Les types de variables sont string, je te laisse le soin de transformer les types
    String heureFromDatabase;
    String matiereFromDatabase;
    String profnomFromDatabase;
    String tdFromDatabase;
    String salleFromDatabase;
    String typeFromDatabase;


    private LinearLayout coursProchainLayout; // Layout englobant tout le texte et définissant la couleur
    private LinearLayout coursSuivantLayout; // Le cours après le prochain cours




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // Access a Cloud Firestore instance from your Activity
        db = new Database();


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
       // mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        Bundle extras = getIntent().getExtras();

        choixLicense = extras.getString("License");
        groupe = extras.getInt("Groupe");


        String fileName = "";
        if (choixLicense.contains("L1")) {
            fileName = "l1.ics";
        } else if (choixLicense.contains("L2")) {
            fileName = "l2.ics";
        } else if (choixLicense.contains("L3")) {
            fileName = "l3.ics";
        }

        Log.d("huhu","VOICI LE GROUPE SLECTIONNE :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: " + groupe);

        // Je recupère le calendrier sous forme d'objet pour récupérer la date sur laquelle il est mis.
        // Par défaut, il se mets à la date de l'OS, donc la date d'aujourd'hui
        Long todayDate;
        /*
        Récupérer la date actuelle : String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
         */

        try {
            Parser.getValues(getAssets().open(fileName));
            Database.printDb();
        } catch (IOException e) {
            e.printStackTrace();
        }

        getNextCourse(groupe);



        gotoCalButton = (Button) findViewById(R.id.gotoCalendarButton);
        gotoExaButton = (Button) findViewById(R.id.gotoExamenButton);
        gotoPrefButton = (Button) findViewById(R.id.gotoPreferenceButton);

        gotoCalButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, CalendarActivity.class);
                intent.putExtra("License",choixLicense);
                intent.putExtra("Groupe", groupe);
                startActivity(intent);
            }
        });


        gotoExaButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ExamenActivity.class);
                intent.putExtra("License",choixLicense);
                intent.putExtra("Groupe", groupe);
                startActivity(intent);
            }
        });

        gotoPrefButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, OptionsActivity.class);
                intent.putExtra("License",choixLicense);
                intent.putExtra("Groupe", groupe);
                startActivity(intent);
            }
        });

    }

    public void displayToast(View v, String course) {
        Toast.makeText(MainActivity.this, "Vous etes en retard en " + course, Toast.LENGTH_SHORT).show();
    }

    public void remplirHoraire(LinearLayout horaireLayout, String heuretype, String matiere, String profnom, String td, String salle, String type) {

        TextView tv;

        for (int i = 1 ; i < 7 ; i++) {
            tv = (TextView) horaireLayout.getChildAt(i);
            switch(i) {
                case 1 : tv.setText(heuretype); break;
                case 2 : tv.setText(matiere); break;
                case 3 : tv.setText(profnom); break;
                case 4 : tv.setText(td); break;
                case 5 : tv.setText(salle); break;
                case 6 : tv.setText(type); break;
            }
        }
        tv = (TextView) horaireLayout.getChildAt(0);
        tv.setText("Votre prochain cours");
    }


    public void horaireVide(LinearLayout horaireLayout) {

        TextView tv;

        for (int i = 1 ; i < 7 ; i++) {
            tv = (TextView) horaireLayout.getChildAt(i);
            tv.setText("");
        }

        tv = (TextView) horaireLayout.getChildAt(0);
        tv.setText("Aucun cours prévu");

    }

    public void getNextCourse(int groupe) {
        Date now = new Date();
        afficherHoraires(db.getNextCourse(groupe));
    }

    public void afficherHoraires(ArrayList<Course> results) {

        LinearLayout ll = (LinearLayout) findViewById(R.id.lin);

        // Je récupère tous les layout en tant qu'objet modifiable
        coursProchainLayout = (LinearLayout) findViewById(R.id.coursProchainLayout);
        coursSuivantLayout = (LinearLayout) findViewById(R.id.coursSuivantLayout);

        // Je les mets dans une ArrayList, il n'y a plus qu'à la parcourir dans un for pour remplir chaque cours dans l'ordre croissant des horaires
        ArrayList<LinearLayout> allCours = new ArrayList<LinearLayout>();
        allCours.add(coursProchainLayout); allCours.add(coursSuivantLayout);

        if (results.get(0).dtstart.compareTo(new Date()) <= -1) {
            displayToast(ll, results.get(0).ue);
        }

        for (int i = 0; i < results.size() ; i++) {

            Course result = results.get(i);

            //Ici, il faut écrire ce qu'il faudra pour récupérer les données du cours en question et remplir les [machin]FromDatabase


            Date dateS = result.dtstart;
            Date dateN = result.dtend;

            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            String strDate = dateFormat.format(dateS);
            String[] split = strDate.split(" ");


            String strDate2 = dateFormat.format(dateN);
            String[] split2 = strDate2.split(" ");


            String heureFromDatabase = split[1] + " - "  + split2[1];
            String matiereFromDatabase = result.ue;
            String profnomFromDatabase = result.prof;
            String tdFromDatabase = result.groups.toString();
            String salleFromDatabase = result.location;
            String typeFromDatabase = result.type;

            if (heureFromDatabase == null) {
                heureFromDatabase = " ";
            }

            if (matiereFromDatabase == null) {
                matiereFromDatabase = " ";
            }

            if (profnomFromDatabase == null) {
                profnomFromDatabase = " ";
            }

            if (tdFromDatabase == null) {
                tdFromDatabase = " ";
            }

            if (salleFromDatabase == null) {
                salleFromDatabase = " ";
            }

            if (typeFromDatabase == null) {
                typeFromDatabase = " ";
            }

            String heuretypeComplet = heureFromDatabase;
            String matiereComplet = "Matiere : " + matiereFromDatabase;
            String profnomComplet = "Enseignant : " + profnomFromDatabase;
            String tdComplet = "Groupe : " + tdFromDatabase;
            String salleComplet = "Salle : " + salleFromDatabase;
            String typeComplet = "Type : " + typeFromDatabase;

            remplirHoraire(allCours.get(i),heuretypeComplet,matiereComplet,profnomComplet,tdComplet,salleComplet,typeComplet);
        }
    }

    }