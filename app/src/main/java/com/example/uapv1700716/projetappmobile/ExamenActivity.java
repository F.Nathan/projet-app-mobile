package com.example.uapv1700716.projetappmobile;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ExamenActivity extends AppCompatActivity {

    // Variables pour les extras
    String choixLicense;
    int groupe;


    private LinearLayout cours1;
    private LinearLayout cours2;
    private LinearLayout cours3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_examen);


        // Récupération des Extras
        Bundle extras = getIntent().getExtras();

        choixLicense = extras.getString("License");
        groupe = extras.getInt("Groupe");

        afficherHoraires();



    }

    public void remplirHoraire(LinearLayout horaireLayout, String heuretype, String matiere, String profnom, String td, String salle, String type) {
        horaireLayout.setBackgroundResource(R.drawable.black_border_green_within);

        TextView tv;

        for (int i = 0 ; i < 6 ; i++) {
            tv = (TextView) horaireLayout.getChildAt(i);
            switch(i) {
                case 0 : tv.setText(heuretype); break;
                case 1 : tv.setText(matiere); break;
                case 2 : tv.setText(profnom); break;
                case 3 : tv.setText(td); break;
                case 4 : tv.setText(salle); break;
                case 5 : tv.setText(type); break;
            }
        }
    }


    public void horaireVide(LinearLayout horaireLayout) {
        horaireLayout.setBackgroundResource(R.drawable.border);

        TextView tv;

        for (int i = 0 ; i < 6 ; i++) {
            tv = (TextView) horaireLayout.getChildAt(i);
            tv.setText("");
        }
    }


    public void afficherHoraires() {

        /* Accéder à la database grâce à la date et remplir les */
        ArrayList<Course> courses = Database.getEvaluations(groupe);

        Boolean remplir = true;

        // Je récupère tous les layout en tant qu'objet modifiable
        cours1 = (LinearLayout) findViewById(R.id.cours1);
        cours2 = (LinearLayout) findViewById(R.id.cours2);
        cours3 = (LinearLayout) findViewById(R.id.cours3);

        horaireVide(cours1);
        horaireVide(cours2);
        horaireVide(cours3);

        // Je les mets dans une ArrayList, il n'y a plus qu'à la parcourir dans un for pour remplir chaque cours dans l'ordre croissant des horaires
        ArrayList<LinearLayout> allCours = new ArrayList<LinearLayout>();

        allCours.add(cours1);
        allCours.add(cours2);
        allCours.add(cours3);

        for (int i = 0; i < courses.size(); i++) {

            Course result = courses.get(i);

            //Ici, il faut écrire ce qu'il faudra pour récupérer les données du cours en question et remplir les [machin]FromDatabase


            Date dateS = result.dtstart;
            Date dateN = result.dtend;

            DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
            String strDate = dateFormat.format(dateS);
            String[] split = strDate.split(" ");


            String strDate2 = dateFormat.format(dateN);
            String[] split2 = strDate2.split(" ");


            String heureFromDatabase = split[1] + " - "  + split2[1];
            String matiereFromDatabase = result.ue;
            String profnomFromDatabase = result.prof;
            String tdFromDatabase = result.groups.toString();
            String salleFromDatabase = result.location;
            String typeFromDatabase = result.type;


            if (remplir == true) {
                String heuretypeComplet = heureFromDatabase;
                String matiereComplet = "Matiere : " + matiereFromDatabase;
                String profnomComplet = "Enseignant : " + profnomFromDatabase;
                String tdComplet = "TD : " + tdFromDatabase;
                String salleComplet = "Salle : " + salleFromDatabase;
                String typeComplet = "Type : " + typeFromDatabase;
                remplirHoraire(allCours.get(i), heuretypeComplet, matiereComplet, profnomComplet, tdComplet, salleComplet, typeComplet);
            } else {
                horaireVide(allCours.get(i));
            }
        }

    }
}
