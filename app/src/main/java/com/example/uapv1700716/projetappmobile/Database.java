package com.example.uapv1700716.projetappmobile;


import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {

    public static Map<Date, Course> data;

    public Database() {

        data = new HashMap<Date, Course>(0);
    }

    public static void insertData(Course c) {
        if (c.dtstart != null) {
            Database.data.put(c.dtstart, c);
        }
    }

    public static void printDb() {
        Log.d("XXXXXXXXXXX", Arrays.toString(data.entrySet().toArray()));
    }


    public ArrayList<Course> getNextCourse(int groupe) {
        List<Date> dates = new ArrayList<>(data.keySet());
        Collections.sort(dates);

        Date current = new Date();


        Date late;
        Calendar cal = Calendar.getInstance();
        cal.setTime(current);
        cal.add(Calendar.MINUTE, -15);
        late = cal.getTime();
        Log.d("DATE", late.toString());

        ArrayList<Course> courses = new ArrayList<>(0);

        for (Date d: dates) {
            if ((d.compareTo(late) >= 1) && ( data.get(d).groups.get(0).contains("TD" +  Integer.toString(groupe)) || data.get(d).groups.get(0).contains("INFORMATIQUE"))) {

                Log.d("DATE", d + " ff " + data.get(d).toString());
                courses.add(data.get(d));

                if (courses.size() == 2) {
                    break;
                }
            }
        }

        return courses;

    }

    public static  ArrayList<Course> getDayCourses(Date date, int groupe) {

        List<Date> dates = new ArrayList<>(data.keySet());
        Collections.sort(dates);

        Date tom;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 1); //minus number would decrement the days
        tom = cal.getTime();

        ArrayList<Course> courses = new ArrayList<>(0);

        for (Date d: dates) {
            if (d.compareTo(date) >= 1  && d.compareTo(tom) <= 1 && ( data.get(d).groups.get(0).contains("TD" +  Integer.toString(groupe)) || data.get(d).groups.get(0).contains("INFORMATIQUE"))) {

                Log.d(" ### DATE JOUR ###", d + " ff " + data.get(d).toString());
                courses.add(data.get(d));

            }

            if (d.compareTo(tom) >= 1) {
                return courses;
            }
        }

        return null;
    }

    public static  ArrayList<Course> getEvaluations(int groupe) {

        List<Date> dates = new ArrayList<>(data.keySet());
        Collections.sort(dates);

        Date date = new Date();

        ArrayList<Course> courses = new ArrayList<>(0);

        for (Date d: dates) {
            if (d.compareTo(date) >= 1  && ( data.get(d).groups.get(0).contains("TD" +  Integer.toString(groupe)) || data.get(d).groups.get(0).contains("INFORMATIQUE"))) {

                if (data.get(d).type.contains("Eval")) {
                    Log.d(" ### DATE JOUR ###", d + " ff " + data.get(d).toString());
                    courses.add(data.get(d));
                }


            }

            if (courses.size() == 3) {
                break;
            }
        }

        return courses;
    }

    /*

    public ArrayList<Map<String, Object>> getByDate() {
        Date now = new Date();
        DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        Date today = new Date();
        Date tom = new Date();

        try {
            now = formatter.parse(formatter.format(today));
            tom = formatter.parse(formatter.format(today));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        Log.d("ZXZXZXZXZZX", now.toString());


        Calendar c = Calendar.getInstance();
        c.setTime(tom);
        c.add(Calendar.DATE, 1);
        tom = c.getTime();



        ArrayList<Map<String, Object>> result = new ArrayList<Map<String, Object>>();


        firestore.collection("Calendar")
                .whereEqualTo("GROUPS", "L3INFO_TD3")
                .whereGreaterThan("DTSTART", now)
                .whereLessThan("DTSTART", tom)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("#", document.getId() + " => " + document.getData());
                                result.add(document.getData());
                            }
                        } else {
                            Log.d("#", "Error getting documents: ", task.getException());
                        }
                    }
                });

        return result;
    }


        // Create a reference to the cities collection
        CollectionReference cal = db.collection("Calendar");

        Query query = cal.whereEqualTo("DTSTART", "27/04/2019");
        query.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d("#", document.getId() + " => " + document.getData());
                    }
                } else {
                    Log.d("#", "Error getting documents: ", task.getException());
                }
            }
        });
        */



        /*
        try {
            Parser.getValues(getAssets().open("l3.ics"), db);
        } catch (IOException e) {
            e.printStackTrace();
        }
        */


}
