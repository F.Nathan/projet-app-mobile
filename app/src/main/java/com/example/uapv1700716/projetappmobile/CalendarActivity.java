package com.example.uapv1700716.projetappmobile;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.icu.util.Calendar;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarActivity extends AppCompatActivity {

    // Variables pour les extras
    String choixLicense;
    int groupe;


    // Variable boolean pour savoir si le cours existe, s'il n'y a pas de cours à l'horaire donnée, elle est false
    // Vu que mon emploi du temps par jour n'est pas modulable, il faut parcourir toutes les cases d'horaire à chaque fois
    // Si le cours existe à l'horaire, il faut passer coursARemplir à true et récupérer les info de la database, sinon juste passer coursARemplir a false
    public boolean coursARemplir;

    // Ce sont toutes les data que tu dois saisir depuis la database pour une horaire précise
    // Il n'y a pas besoin de l'heure car c'est déjà préremplis
    // Les types de variables sont string, je te laisse le soin de transformer les types
    String matiereFromDatabase;
    String profnomFromDatabase;
    String tdFromDatabase;
    String salleFromDatabase;
    String typeFromDatabase;

    private CalendarView cv;


    // horaire 1 - 8:30 - 10:00
    private LinearLayout cours1; // Layout englobant tout le texte et définissant la couleur

    // horaire 2 - 10:00 - 11:30
    private LinearLayout cours2;

    // horaire 3 - 11:30 - 13:00
    private LinearLayout cours3;

    // horaire 4 - 13:00 - 14:30
    private LinearLayout cours4;

    // horaire 5 - 14:30 - 16:00
    private LinearLayout cours5;

    // horaire 6 - 16:00 - 17:30
    private LinearLayout cours6;

    // horaire 7 - 17:30 - 19:00
    private LinearLayout cours7;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        // Récupération des Extras
        Bundle extras = getIntent().getExtras();

        choixLicense = extras.getString("License");
        groupe = extras.getInt("Groupe");


        // Je recupère le calendrier sous forme d'objet pour récupérer la date sur laquelle il est mis.
        // Par défaut, il se mets à la date de l'OS, donc la date d'aujourd'hui
        cv = (CalendarView) findViewById(R.id.calendarView);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date(cv.getDate());

        afficherHoraires(date);




        // Permet de lancer des actions quand tu selectionnes une date sur le calendrier (comme un SetOnclickListener)
        cv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {

                Date date = new GregorianCalendar( year, month, dayOfMonth ).getTime();
                Log.d(" ### DATE JOUR ###", date.toString());
                afficherHoraires(date);
            }
        });

    }


    public void remplirHoraire(LinearLayout horaireLayout, String heuretype, String matiere, String profnom, String td, String salle, String type) {
        horaireLayout.setBackgroundResource(R.drawable.black_border_green_within);

        TextView tv;

        for (int i = 0 ; i < 6 ; i++) {
            tv = (TextView) horaireLayout.getChildAt(i);
            switch(i) {
                case 0 : tv.setText(heuretype); break;
                case 1 : tv.setText(matiere); break;
                case 2 : tv.setText(profnom); break;
                case 3 : tv.setText(td); break;
                case 4 : tv.setText(salle); break;
                case 5 : tv.setText(type); break;
            }
        }
    }


    public void horaireVide(LinearLayout horaireLayout) {
        horaireLayout.setBackgroundResource(R.drawable.borderless);

        TextView tv;

        for (int i = 0 ; i < 6 ; i++) {
            tv = (TextView) horaireLayout.getChildAt(i);
            tv.setText("");
        }
    }


    public void afficherHoraires(Date date) {

        /* Accéder à la database grâce à la date et remplir les */
        ArrayList<Course> courses = Database.getDayCourses(date, groupe);



        // Par défaut, il n'y a pas de cours
        coursARemplir = true;

        // Je récupère tous les layout en tant qu'objet modifiable
        cours1 = (LinearLayout) findViewById(R.id.cours1);
        cours2 = (LinearLayout) findViewById(R.id.cours2);
        cours3 = (LinearLayout) findViewById(R.id.cours3);
        cours4 = (LinearLayout) findViewById(R.id.cours4);
        cours5 = (LinearLayout) findViewById(R.id.cours5);
        cours6 = (LinearLayout) findViewById(R.id.cours6);
        cours7 = (LinearLayout) findViewById(R.id.cours7);

        horaireVide(cours1);
        horaireVide(cours2);
        horaireVide(cours3);
        horaireVide(cours4);
        horaireVide(cours5);
        horaireVide(cours6);
        horaireVide(cours7);

        // Je les mets dans une ArrayList, il n'y a plus qu'à la parcourir dans un for pour remplir chaque cours dans l'ordre croissant des horaires
        ArrayList<LinearLayout> allCours = new ArrayList<LinearLayout>();
        allCours.add(cours1); allCours.add(cours2); allCours.add(cours3); allCours.add(cours4); allCours.add(cours5); allCours.add(cours6); allCours.add(cours7);

        if (courses == null) {
            return;
        } else {
            for (int i = 0 ; i < courses.size() ; i++) {

                Course result = courses.get(i);

                //Ici, il faut écrire ce qu'il faudra pour récupérer les données du cours en question et remplir les [machin]FromDatabase


                Date dateS = result.dtstart;
                Date dateN = result.dtend;

                DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
                String strDate = dateFormat.format(dateS);
                String[] split = strDate.split(" ");


                String strDate2 = dateFormat.format(dateN);
                String[] split2 = strDate2.split(" ");


                String heureFromDatabase = split[1] + " - "  + split2[1];
                String matiereFromDatabase = result.ue;
                String profnomFromDatabase = result.prof;
                String tdFromDatabase = result.groups.toString();
                String salleFromDatabase = result.location;
                String typeFromDatabase = result.type;


                if (coursARemplir == true) {
                    String heuretypeComplet = heureFromDatabase;
                    String matiereComplet = "Matiere : " + matiereFromDatabase;
                    String profnomComplet = "Enseignant : " + profnomFromDatabase;
                    String tdComplet = "TD : " + tdFromDatabase;
                    String salleComplet = "Salle : " + salleFromDatabase;
                    String typeComplet = "Type : " + typeFromDatabase;
                    remplirHoraire(allCours.get(i),heuretypeComplet,matiereComplet,profnomComplet,tdComplet,salleComplet,typeComplet);
                }
                else {
                    horaireVide(allCours.get(i));
                }
            }
        }

    }


}
