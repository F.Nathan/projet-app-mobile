package com.example.uapv1700716.projetappmobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import java.util.ArrayList;

public class SelectionActivity extends AppCompatActivity {

    RadioGroup radioGroup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {




        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        Spinner spinner = (Spinner) findViewById(R.id.choixSpinner);
        String[] lRegion={"L1 Informatique","L2 Informatique","L3 Informatique"};
        ArrayAdapter<String> dataAdapterR = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,lRegion);
        dataAdapterR.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapterR);


        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        int groupeSelection = pref.getInt("Groupe", 1);             // getting Integer
        String licenseSelection = pref.getString("choixLicense", "L1 Informatique");         // getting String

        radioGroup = findViewById(R.id.radioGroup);

        Log.d(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::",radioGroup.getChildCount()+"");

        RadioButton radioButton = (RadioButton) radioGroup.getChildAt(groupeSelection-1);
        radioButton.setChecked(true);

        switch(licenseSelection) {
            case "L1 Informatique" : spinner.setSelection(0); break;
            case "L2 Informatique" : spinner.setSelection(1); break;
            case "L3 Informatique" : spinner.setSelection(2); break;
        }





        Button validerButton = (Button) findViewById(R.id.validerButton);



        validerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String choixLicense = spinner.getSelectedItem().toString();

                radioGroup = findViewById(R.id.radioGroup);

                int groupeSelected = 0;

                Snackbar.make(v, "Veuillez sélectionner un seul groupe", 100).show();

                RadioButton radioButton = findViewById(radioGroup.getCheckedRadioButtonId());

                switch (radioButton.getText().toString()) {
                    case "Groupe 1" : groupeSelected = 1; break;
                    case "Groupe 2" : groupeSelected = 2; break;
                    case "Groupe 3" : groupeSelected = 3; break;
                    case "Groupe 4" : groupeSelected = 4; break;
                }

                /*
                if (groupeSelected == 0) {
                    Snackbar.make(v, "Veuillez sélectionner un seul groupe", 100).show();
                    return;
                }
                */

                // Création de l'intent et start de MainActivity
                Intent intent = new Intent(SelectionActivity.this , MainActivity.class);
                intent.putExtra("License",choixLicense); // Permet de récupérer la license
                intent.putExtra("Groupe", groupeSelected);
                startActivity(intent);

            }
        });
    }
}
