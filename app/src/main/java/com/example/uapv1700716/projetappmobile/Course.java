package com.example.uapv1700716.projetappmobile;

import java.util.ArrayList;
import java.util.Date;

public class Course {
    public String categories;
    public Date dtstamp;
    public Date lastModified;
    public String UID;
    public Date dtstart;
    public Date dtend;
    public String summary;
    public String location;
    public String desc;
    public String ue;
    public String prof;
    public ArrayList<String> groups;
    public String type;

    public Course() {
        groups = new ArrayList<>(0);
        groups.add(" ");
    }


    public String toString() {
        return "Date: '" + this.dtstart + "', UE: '" + this.ue + "', prof: '" + this.prof + "', groups: '" +  this.groups.toString() + "', salle: '" + this.location + "', type: '" + this.type + "'";
    }


}
