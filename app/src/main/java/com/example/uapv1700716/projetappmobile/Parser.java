package com.example.uapv1700716.projetappmobile;

import android.util.Log;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

public class Parser {

    public static void getValues(InputStream file) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(file));
        String strLine;
        Course c = new Course();

        Boolean ignore = false;

        try {
            while ((strLine = reader.readLine()) != null) {

                if (strLine.equals("BEGIN:VEVENT")) {
                    c = new Course();
                    ignore = false;

                } else if (strLine.contains("CATEGORIES")) {
                    //data.put("CATEGORIES", strLine.substring(11));
                    c.categories = strLine.substring(11);

                } else if (strLine.contains("DTSTAMP")) {
                    //data.put("DTSTAMP", Parser.toDate(strLine.substring(8)));
                    c.dtstamp = Parser.toDate(strLine.substring(8));

                } else if (strLine.contains("LAST-MODIFIED")) {
                    //data.put("LAST-MODIFIED", Parser.toDate(strLine.substring(14)));
                    c.lastModified = Parser.toDate(strLine.substring(14));

                } else if (strLine.contains("UID")) {
                    //data.put("UID", strLine.substring(4));
                    c.UID = strLine.substring(4);

                } else if (strLine.contains("DTSTART")) {
                    //data.put("DTSTART", Parser.toDate(strLine.substring(8)));
                    c.dtstart =  Parser.toDate(strLine.substring(8));

                } else if (strLine.contains("DTEND")) {
                    //data.put("DTEND", Parser.toDate(strLine.substring(6)));
                    c.dtend = Parser.toDate(strLine.substring(6));

                } else if (strLine.contains("SUMMARY")) {
                    //data.put("SUMMARY", strLine.substring(20));
                    c.summary = strLine.substring(20);
                    if (c.summary.contains("Annulation")) {
                        ignore = true;
                    }

                    Parser.parseSummary(c, strLine.substring(20));

                } else if (strLine.contains("LOCATION")) {
                    //data.put("LOCATION", strLine.substring(21));
                    c.location = strLine.substring(21);

                } else if (strLine.contains("DESCRIPTION")) {
                    //data.put("DESCRIPTION", strLine.substring(23));
                    c.desc = strLine.substring(23);

                } else if (strLine.equals("END:VEVENT")) {
                    Log.d("ZZWEZZZ", c.toString());
                    if (!ignore) {
                        Database.insertData(c);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static Date toDate (String date) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
            Date parsedDate = dateFormat.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(parsedDate);
            cal.add(Calendar.HOUR, 2);
            parsedDate = cal.getTime();
            return parsedDate;
        } catch(Exception e) {
            Log.d("## ?> db || ", "convertion to timeStamp failed" + e);
            return null;
        }
    }


    public static void parseSummary(Course c, String sum) {

        Boolean course = true;
        Boolean prof = true;
        Boolean group = true;
        int id = 0;


        for (int i = 0; i < sum.length(); i++) {
            if (sum.charAt(i) == '-') {
                if (course) {
                    //data.put("UE", sum.substring(0, i));
                    c.ue = sum.substring(0, i);
                    id = i + 1;
                    course = false;
                } else if (prof) {
                    //data.put("PROF", sum.substring(id, i));
                    c.prof = sum.substring(id, i);
                    id = i + 1;
                    prof = false;
                } else if (group) {
                    String sub = sum.substring(id, i);
                    if (sub.contains("\\\\")) {
                        String[] parts = sub.split("\\\\");
                        c.groups = new ArrayList<>(Arrays.asList(parts));
                    } else {
                        ArrayList<String> groups = new ArrayList<>();
                        groups.add(sub);
                        c.groups = groups;
                    }

                    id = i + 1;
                    group = true;

                }

                }
            if (i+1 == sum.length()) {
                c.type =  sum.substring(id, i + 1);
                return;
            }
        }

    }
}
